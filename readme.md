#Laravel Vue Demo
Demonstration of fundamental web development concepts using Laravel and Vue.js

##Installation

``` bash

# install php dependencies
$ vuestic myproject

# install js dependencies
$ npm install

# load database with test data
$ php artisan migrate:seed

```

##