<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Data extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // print_r($this->resource['values']);
        // echo $this->resource->lablels
        return [
            'labels' => $this->resource['labels'],
            'values' => $this->resource['values']
        ];
    }
}