<?php

namespace App\Http\Controllers;

use App\Data;
use App\Http\Resources\Data as DataResource;

class DataController extends Controller
{
    public function show ($id)
    {
    	$labels = [];
    	$values = [];
		$all_records = Data::get();
		foreach ($all_records as $record) {
			// echo $record;
			array_push($labels, $record['label']);
			array_push($values, $record['value']);
		}
        $data = array(
        	'labels' => $labels,
        	'values' => $values
        );
        return new DataResource($data);
    }
}