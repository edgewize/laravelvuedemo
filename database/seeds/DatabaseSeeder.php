<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Eloquent::unguard();

        //call uses table seeder class
    		$this->call('DataTableSeeder');
        //this message shown in your terminal after running db:seed command
        $this->command->info("Users table seeded :)");
       
    }
}

class DataTableSeeder extends Seeder {
 
       public function run()
       {
         //delete users table records
         DB::table('data')->delete();
         //insert some dummy records
         DB::table('data')->insert(array(
             array('label'=>'JAN','value'=> 14),
             array('label'=>'FEB','value'=> 16),
             array('label'=>'MAR','value'=> 11),
             array('label'=>'APR','value'=> 10),
             array('label'=>'MAY','value'=> 6),
             array('label'=>'JUN','value'=> 13)

          ));
       }
 
}