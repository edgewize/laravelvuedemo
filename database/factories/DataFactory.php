<?php

use Faker\Generator as Faker;

$factory->define(App\Data::class, function (Faker $faker) {
    return [
        'id' => '0',
        'label' => str_random(10),
        'value' => rand(0,25)
    ];
});
